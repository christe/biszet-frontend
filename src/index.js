// js
import 'jquery';
import 'tether';
import 'popper.js';
import 'slick-carousel';
import 'hamburgers';
import 'vide';

import './bootstrap/bootstrap';

// sass
import './bootstrap/bootstrap.scss';
import '../node_modules/slick-carousel/slick/slick.scss';
import '../node_modules/slick-carousel/slick/slick-theme.scss';

import './style.scss';

// html
import './index.html';
import './references.html';
import './imprint-legal.html';
import './contact.html';
import './benefits.html';
import './products.html';

// images
import './img/logos/biszet/logo_biszet_4c.svg';
import './img/logos/biszet/logo_biszet_w.svg';
import './img/logos/biszet/logo_B7.svg';
import './img/logos/biszet/logo_B11.svg';

import './img/siegel.png';
import './img/home/540x365_home_product_01.jpg';
import './img/home/540x365_home_product_02.jpg';
import './img/home/540x365_home_product_03.jpg';
import './img/home/540x365_home_product_04.jpg';
import './img/home/764x435_home_01.jpg';
import './img/home/652x298_home_01.jpg';
import './img/illu_cosmetics.svg';

import './img/logos/presse/presse_ad.png';
import './img/logos/presse/presse_elle.png';
import './img/logos/presse/presse_handelsblatt.png';
import './img/logos/presse/presse_house-garden.png';
import './img/logos/presse/presse_vogue.png';
import './img/logos/presse/presse_wallpaper.png';

// reference images
import './img/logos/references/references_bsc.png';
import './img/logos/references/references_budersand.png';
import './img/logos/references/references_colourliving.png';
import './img/logos/references/references_faehrhaus-sylt.png';
import './img/logos/references/references_jumeirah.png';
import './img/logos/references/references_park-hyatt.png';
import './img/logos/references/references_ritz.png';
import './img/logos/references/references_the-shard.png';

// benefits
import './img/benefits/652x298_benefits_01.jpg';
import './img/benefits/652x298_benefits_02.jpg';
import './img/benefits/764x435_benefits_01.jpg';

// products
import './img/product/764x435_product_01.jpg';
import './img/product/764x435_product_02.jpg';
import './img/product/764x435_product_03.jpg';
import './img/product/764x435_product_04.jpg';
import './img/product/764x435_product_05.jpg';

import './img/product/365x540_product_02.jpg';
import './img/product/540x365_product_01.jpg';
import './img/product/540x365_product_03.jpg';

import './img/tech_illu_b7.svg';
import './img/tech_illu_b11.svg';

import './img/biszet_bg_freisteller/freisteller/b7_closed_side.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_335.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_335@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_768.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_768@2x.png';
import './img/biszet_bg_freisteller/freisteller/b11_b7_opened.png';
import './img/biszet_bg_freisteller/freisteller/b11_b7_opened_768.png';
import './img/biszet_bg_freisteller/freisteller/b11_b7_opened_768@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_b11_closed.png';
import './img/biszet_bg_freisteller/freisteller/b7_b11_closed_768.png';
import './img/biszet_bg_freisteller/freisteller/b7_b11_closed_768@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_768.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_768@2x.png';
import './img/biszet_bg_freisteller/backgrounds/bg_home_references_v2.jpg';
import './img/biszet_bg_freisteller/backgrounds/1920x860_home.jpg';
import './img/biszet_bg_freisteller/backgrounds/bg_cta_v2.jpg';
import './img/biszet_bg_freisteller/backgrounds/bg_benefits_quote_v2.jpg';
import './img/biszet_bg_freisteller/backgrounds/bg_benefits_cosmetics_v2.jpg';
import './img/biszet_bg_freisteller/backgrounds/1920x860_benefits.jpg';
import './img/biszet_bg_freisteller/backgrounds/bg_cta.png';
import './img/biszet_bg_freisteller/backgrounds/bg_products_head.jpg';

// mobile bgs
import './img/biszet_bg_freisteller/bg_mobile/768_benefits.jpg';
import './img/biszet_bg_freisteller/bg_mobile/768_benefits@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/768_home.jpg';
import './img/biszet_bg_freisteller/bg_mobile/768_home@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_benefits_cosmetics_768.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_benefits_cosmetics_768@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_benefits_quote_768.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_benefits_quote_768@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_cta_768.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_cta_768@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_home_references_768.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_home_references_768@2x.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_products_head_768.jpg';
import './img/biszet_bg_freisteller/bg_mobile/bg_products_head_768@2x.jpg';


// mobile freisteller
import './img/biszet_bg_freisteller/freisteller/b7_b11_closed_335.png';
import './img/biszet_bg_freisteller/freisteller/b7_b11_closed_335@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_335.png';
import './img/biszet_bg_freisteller/freisteller/b7_closed_side_335@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_contact_335.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_contact_335@2x.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_335.png';
import './img/biszet_bg_freisteller/freisteller/b7_opened_335@2x.png';
import './img/biszet_bg_freisteller/freisteller/b11_b7_opened_335.png';
import './img/biszet_bg_freisteller/freisteller/b11_b7_opened_335@2x.png';



import './video/biszet_background.mp4';

$(function() {

  $('.slick').slick({
    arrows: false,
    dots: true
  });

  $('.slick-products').slick({
    arrows: false,
    dots: false,
    fade: true,
    slidesToScroll: 1,
    asNavFor: '.slick-products-small'
  });

  $('.slick-products-small').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    asNavFor: '.slick-products',
    focusOnSelect: true
  });

  $('.header .hamburger').click(function(e) {

    e.preventDefault();

    $(this).toggleClass('is-active');

    if ($(this).hasClass('is-active')) {
      $('.header').addClass('open');
    } else {
      $('.header').removeClass('open');
    }
  });

  $('.module-home-references .logos img').hover(function() {
    $('#quoter').html($(this).data('quoter'));
    $('#quote').html($(this).data('quote'));
  });

});
