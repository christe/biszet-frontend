var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
    context: path.resolve(__dirname, "src"),
    entry: {
        app: './index'
    },
    output: {
        path: path.resolve('dist'),
        filename: '[name].js?[hash]'
    },
    devServer: {
        contentBase: './dist'
    },
    resolve: {
        extensions: ['.webpack.js', '.web.js', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.js$/, loaders: ['jsx-loader', 'babel-loader'], exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css-loader!sass-loader')
            },
            {
                test: /\.(html)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif|svg|mp4)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            useRelativePath: true
                        }
                    }
                ]
            },
            {
                test: /\.(eot|woff|ttf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true,
            disable: false
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',         // for Bootstrap 3.x / 4.x
            'window.jQuery': 'jquery',  // for Bootstrap 3.x / 4.x
            'Tether': 'tether',         // for Bootstrap 4.x
            'window.Tether': 'tether',  // for Bootstrap 4.x
            'Popper': 'popper.js'
        })
    ]
};

module.exports = config;